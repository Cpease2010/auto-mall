package com.tmo.automall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoMallApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoMallApplication.class, args);
	}

}
